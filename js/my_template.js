//我的模版

(function(){
	window.my_template = {
		//创建item模版  参数2是商品类别
		itemTmpl:function(data, leibie){
			var song_style = data.shipping_fee;
			if(song_style==0){
				song_style = '免费配送';
			}else{
				song_style += '元配送费'
			}
			var distance = data.distance;
			if(distance>=1000){
				distance = (distance/1000).toFixed(1);
				distance = distance+'km';
			}else{
				distance = distance+'m';
			}
			var tmpl = '<div class="wm-list-div" id_data='+data.id+' leibie='+leibie+'>'
				    	+'	<div class="shopInfo">'
				    	+'		<div class="wm-list-div-l">'
					    +'			<img src="'+data.img+'" alt="" />'
					    +'		</div>'
					    +'		<div class="wm-list-div-r">'
					    +'			<div class="wm-list-div-r-t wm-list-div-r-comm">'
					    +'				<h4 class="shop_name">'+data.title+'</h4>'
					    +'				<h5><span>￥</span><span class="orange-txt price">'+data.price+'</span>元起送</h5>'
					    +'			</div>'
					    +'			<div class="wm-list-div-r-m wm-list-div-r-comm">'
					    +'				<span class="stars_comment">'
					    +'					<span class="mui-icon mui-icon-star-filled star-style start-active"></span>'
					    +'					<span class="mui-icon mui-icon-star-filled star-style start-active"></span>'
					    +'					<span class="mui-icon mui-icon-star-filled star-style start-active"></span>'
					    +'					<span class="mui-icon mui-icon-star-filled star-style start-active"></span>'
					    +'					<span class="mui-icon mui-icon-star-filled star-style"></span>'
					    +'				</span>'
					    +'				<span class="small-txt">4.5</span>'
					    +'				<span class="small-txt">月售'+data.buy_number+'件</span>'
					    +'				<span class="send_style small-txt">'+song_style+'</span>'
					    +'			</div>'
					    +'			<div class="wm-list-div-r-b wm-list-div-r-comm">'
					    +'				<span class="small-txt">大约'+data.time+'分钟</span>'
					    +'				<span class="small-txt">'+distance+'</span>'
					    +'			</div>'
				    	+'		</div>'
				    	+'	</div>'
				    	+' </div>';
				    	
			return tmpl;
		}
	}
	
	
	
})();
